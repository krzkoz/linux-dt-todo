# SPDX-License-Identifier: GPL-2.0-only
# Copyright 2022 Linaro Ltd

import re

def load_todos(path):
    todos = {}
    with open(path) as fp:
        while True:
            line = fp.readline()
            if not line:
                break
            if not line.startswith('- '):
                raise ValueError('Line does not start with -: {}'.format(line))
            key = line[2:].strip()

            line = fp.readline()
            if not line:
                raise ValueError('Missing second line for item: {}'.format(key))
            status = line.strip()
            todos[key] = status
    return todos

def parse_dtbs_warnings(fp, dir_marker, platform):
    marker = dir_marker + platform + ':'
    marker_found = False
    warnings = []
    while True:
        line = fp.readline()
        if not line:
            break
        if not line.startswith(marker):
            continue

        marker_found = True
        while True:
            line = fp.readline()
            if not line:
                return warnings
            line = line.strip()
            if line == '':
                return warnings
            matches = re.match(r'^[0-9]+  (.+)$', line)
            if not matches:
                raise ValueError('Could not match warning from line: {}'.format(line))
            warnings.append(matches.group(1))

    if not marker_found:
        raise ValueError('Could not find marker for DTBS warnings: {}'.format(marker))

    return warnings

def parse_dtbs_compatibles(fp, dir_marker, platform):
    marker = dir_marker + platform + ':'
    marker_found = False
    compatibles = []
    while True:
        line = fp.readline()
        if not line:
            break
        if not line.startswith(marker):
            continue

        marker_found = True
        while True:
            line = fp.readline()
            if not line:
                return compatibles
            line = line.strip()
            if line == '':
                return compatibles
            compatibles.append(line)

    if not marker_found:
        raise ValueError('Could not find marker for compatibles: {}'.format(marker))

    return compatibles

def parse_platform_warnings(path, dir_marker, platform):
    marker = dir_marker + platform + ':'
    warnings = []
    compatibles = []

    with open(path) as fp:
        warnings = parse_dtbs_warnings(fp, dir_marker, platform)
        compatibles = parse_dtbs_compatibles(fp, dir_marker, platform)

    return (compatibles, warnings)

def update_todos(todos, logs):
    new_todos = {}
    for todo in logs:
        if todo in todos:
            new_todos[todo] = todos[todo]
        else:
            new_todos[todo] = 'NEW'
    return new_todos
