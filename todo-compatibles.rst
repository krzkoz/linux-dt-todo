- ['ad,ad7147_captouch']
  ignore
- ['dlg,da7280']
  ignore
- ['focaltech,fts8719']
  Caleb, https://lore.kernel.org/all/20220123173650.290349-2-caleb@connolly.tech/
- ['google,cr50']
  NEW
- ['jdi,fhd-nt35596s']
  https://lore.kernel.org/all/20220728023555.8952-2-mollysophia379@gmail.com/
- ['lattice,ice40-fpga-mgr']
  ignore
- ['linux,extcon-usb-gpio']
  ignore
- ['mx25l25635e']
  NEW, spi-nor chip
- ['qcom,hfpll']
  NEW
- ['qcom,idle-state-spc', 'arm,idle-state']
  NEW
- ['qcom,idle-state-spc']
  NEW
- ['qcom,kryo465']
  NEW
- ['qcom,pm8058-keypad']
  NEW
- ['qcom,pm8921-keypad']
  NEW
- ['realtek,rt5682i']
  ignore
- ['samsung,s6e3fa2']
  https://lore.kernel.org/all/20210725140339.2465677-1-alexeymin@postmarketos.org/
- ['samsung,s6sy761']
  ignore
- ['samsung,sofef01-m-ams597ut01']
  NEW
- ['swir,mangoh-iotport-spi']
  NEW
- ['ti,lm3697']
  NEW
- ['ti,tas2552']
  ignore
- ['ti,tps65132']
  ignore
- ['truly,nt35597-2K-display']
  ignore
- ['visionox,rm69299-shift']
  Caleb, https://lore.kernel.org/all/20220123173650.290349-4-caleb@connolly.tech/
