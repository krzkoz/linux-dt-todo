- /: memory: False schema does not allow {'device_type': ['memory'], 'reg': [[0, 0]]}
  NEW
- domain-idle-states: cluster-sleep-0: 'idle-state-name', 'local-timer-stop' do not match any of the regexes: 'pinctrl-[0-9]+'
  NEW
- etf@6047000: Unevaluated properties are not allowed ('in-ports' was unexpected)
  NEW
- etf@6047000: in-ports: '#address-cells', '#size-cells', 'port@1' do not match any of the regexes: 'pinctrl-[0-9]+'
  NEW
- idle-states: 'spc' does not match any of the regexes: '^(cpu|cluster)-', 'pinctrl-[0-9]+'
  NEW
- l2-cache: Unevaluated properties are not allowed ('qcom,saw' was unexpected)
  NEW
- mailbox@17911000: compatible: 'oneOf' conditional failed, one must be fixed:
  NEW
- pci@1c00000: iommu-map: [[0, 49, 7184, 1], [256, 49, 7185, 1], [512, 49, 7186, 1], [768, 49, 7187, 1], [1024, 49, 7188, 1], [1280, 49, 7189, 1], [1536, 49, 7190, 1], [1792, 49, 7191, 1], [2048, 49, 7192, 1], [2304, 49, 7193, 1], [2560, 49, 7194, 1], [2816, 49, 7195, 1], [3072, 49, 7196, 1], [3328, 49, 7197, 1], [3584, 49, 7198, 1], [3840, 49, 7199, 1]] is too long
  NEW
- pci@1c08000: iommu-map: [[0, 49, 7168, 1], [256, 49, 7169, 1], [512, 49, 7170, 1], [768, 49, 7171, 1], [1024, 49, 7172, 1], [1280, 49, 7173, 1], [1536, 49, 7174, 1], [1792, 49, 7175, 1], [2048, 49, 7176, 1], [2304, 49, 7177, 1], [2560, 49, 7178, 1], [2816, 49, 7179, 1], [3072, 49, 7180, 1], [3328, 49, 7181, 1], [3584, 49, 7182, 1], [3840, 49, 7183, 1]] is too long
  NEW
- phy@1d87000: '#address-cells', '#size-cells', 'phy@1d87400', 'ranges' do not match any of the regexes: 'pinctrl-[0-9]+'
  NEW
- phy@1d87000: '#phy-cells' is a required property
  NEW
- phy@1d87000: 'power-domains' is a required property
  NEW
- phy@7410000: clock-names: ['aux', 'cfg_ahb', 'ref'] is too short
  NEW
- phy@7410000: clocks: [[25, 94], [25, 99], [25, 213]] is too short
  NEW
- phy@88eb000: '#address-cells', '#size-cells', 'phy@88eb200', 'ranges' do not match any of the regexes: 'pinctrl-[0-9]+'
  NEW
- phy@88eb000: '#clock-cells' is a required property
  NEW
- phy@88eb000: '#phy-cells' is a required property
  NEW
- phy@88eb000: 'clock-output-names' is a required property
  NEW
- phy@88eb000: clock-names: ['aux', 'cfg_ahb', 'ref', 'com_aux'] is too short
  NEW
- phy@88eb000: clock-names:1: 'ref' was expected
  NEW
- phy@88eb000: clock-names:2: 'com_aux' was expected
  NEW
- phy@88eb000: clock-names:3: 'pipe' was expected
  NEW
- phy@88eb000: clocks: [[57, 157], [57, 161], [57, 156], [57, 160]] is too short
  NEW
- phy@88eb000: reset-names:1: 'phy_phy' was expected
  NEW
- phy@c010000: clock-names: ['aux', 'cfg_ahb', 'ref'] is too short
  NEW
- power-controller@f9012000: '#power-domain-cells' is a required property
  NEW
- power-controller@f9012000: 'regulator' does not match any of the regexes: 'pinctrl-[0-9]+'
  NEW
- power-controller@f9012000: compatible: ['qcom,saw2'] is too short
  NEW
- power-controller@f9012000: compatible:0: 'qcom,saw2' is not one of ['qcom,sdm660-gold-saw2-v4.1-l2', 'qcom,sdm660-silver-saw2-v4.1-l2', 'qcom,msm8998-gold-saw2-v4.1-l2', 'qcom,msm8998-silver-saw2-v4.1-l2', 'qcom,msm8909-saw2-v3.0-cpu', 'qcom,msm8916-saw2-v3.0-cpu', 'qcom,msm8939-saw2-v3.0-cpu', 'qcom,msm8226-saw2-v2.1-cpu', 'qcom,msm8974-saw2-v2.1-cpu', 'qcom,msm8976-gold-saw2-v2.3-l2', 'qcom,msm8976-silver-saw2-v2.3-l2', 'qcom,apq8084-saw2-v2.1-cpu', 'qcom,apq8064-saw2-v1.1-cpu']
  NEW
- power-controller@f9089000: '#power-domain-cells' is a required property
  NEW
- power-controller@f9089000: reg: [[4178087936, 4096], [4177563648, 4096]] is too long
  NEW
- power-controller@f9099000: '#power-domain-cells' is a required property
  NEW
- power-controller@f9099000: reg: [[4178153472, 4096], [4177563648, 4096]] is too long
  NEW
- power-controller@f90a9000: '#power-domain-cells' is a required property
  NEW
- power-controller@f90a9000: reg: [[4178219008, 4096], [4177563648, 4096]] is too long
  NEW
- power-controller@f90b9000: '#power-domain-cells' is a required property
  NEW
- power-controller@f90b9000: reg: [[4178284544, 4096], [4177563648, 4096]] is too long
  NEW
- regulator@b0a9000: 'regulator' does not match any of the regexes: 'pinctrl-[0-9]+'
  NEW
- regulator@b0a9000: compatible: ['qcom,saw2'] is too short
  NEW
- regulator@b0a9000: compatible:0: 'qcom,saw2' is not one of ['qcom,sdm660-gold-saw2-v4.1-l2', 'qcom,sdm660-silver-saw2-v4.1-l2', 'qcom,msm8998-gold-saw2-v4.1-l2', 'qcom,msm8998-silver-saw2-v4.1-l2', 'qcom,msm8909-saw2-v3.0-cpu', 'qcom,msm8916-saw2-v3.0-cpu', 'qcom,msm8939-saw2-v3.0-cpu', 'qcom,msm8226-saw2-v2.1-cpu', 'qcom,msm8974-saw2-v2.1-cpu', 'qcom,msm8976-gold-saw2-v2.3-l2', 'qcom,msm8976-silver-saw2-v2.3-l2', 'qcom,apq8084-saw2-v2.1-cpu', 'qcom,apq8064-saw2-v1.1-cpu']
  NEW
- regulator@b0a9000: reg: [[185241600, 4096], [184586240, 4096]] is too long
  NEW
- regulator@b0b9000: 'regulator' does not match any of the regexes: 'pinctrl-[0-9]+'
  NEW
- regulator@b0b9000: compatible: ['qcom,saw2'] is too short
  NEW
- regulator@b0b9000: compatible:0: 'qcom,saw2' is not one of ['qcom,sdm660-gold-saw2-v4.1-l2', 'qcom,sdm660-silver-saw2-v4.1-l2', 'qcom,msm8998-gold-saw2-v4.1-l2', 'qcom,msm8998-silver-saw2-v4.1-l2', 'qcom,msm8909-saw2-v3.0-cpu', 'qcom,msm8916-saw2-v3.0-cpu', 'qcom,msm8939-saw2-v3.0-cpu', 'qcom,msm8226-saw2-v2.1-cpu', 'qcom,msm8974-saw2-v2.1-cpu', 'qcom,msm8976-gold-saw2-v2.3-l2', 'qcom,msm8976-silver-saw2-v2.3-l2', 'qcom,apq8084-saw2-v2.1-cpu', 'qcom,apq8064-saw2-v1.1-cpu']
  NEW
- regulator@b0b9000: reg: [[185307136, 4096], [184586240, 4096]] is too long
  NEW
- remoteproc@4080000: qcom,halt-regs:0: [139] is too short
  NEW
- remoteproc@4080000: qcom,halt-regs:0: [20] is too short
  NEW
- remoteproc@4080000: qcom,halt-regs:0: [43] is too short
  NEW
- rsc@18200000: 'power-domains' is a required property
  NEW
- syscon@f9011000: compatible: 'anyOf' conditional failed, one must be fixed:
  NEW
- usb-phy@9a000: 'reg-names' does not match any of the regexes: 'pinctrl-[0-9]+'
  NEW
- usb-phy@9a000: reset-names: ['por_rst'] is too short
  NEW
- usb-phy@9a000: resets: [[5, 12]] is too short
  NEW
- usb-phy@a6000: 'reg-names' does not match any of the regexes: 'pinctrl-[0-9]+'
  NEW
- usb-phy@a8000: 'reg-names' does not match any of the regexes: 'pinctrl-[0-9]+'
  NEW
- usb@60f8800: clock-names:0: 'core' was expected
  NEW
- usb@76f8800: interrupt-names: ['hs_phy_irq'] is too short
  NEW
- usb@76f8800: interrupts: [[0, 352, 4]] is too short
  NEW
- usb@f92f8800: 'interrupt-names' is a required property
  NEW
- usb@f92f8800: 'oneOf' conditional failed, one must be fixed:
  NEW
